/* eslint-disable @typescript-eslint/no-var-requires */

const fs = require("fs");

function getPackageJson(path = ".") {
  const pkg = fs.readFileSync(path + "/package.json", "utf8");
  return JSON.parse(pkg);
}

module.exports = {
  getPackageJson,
};
