/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-var-requires */

const fs = require("fs");

const { getPackageJson } = require("./common");
const childProcess = require("child_process");

function releasePackage(folderName, isDryRun, onlyVersion, done) {
  process.chdir("packages/" + folderName);

  const pkgName = getPackageJson().name;
  const args = [
    "release-it",
    "--config",
    "../../.release-it.json",
    "--git.tagName=" + pkgName + "-${version}",
    onlyVersion ? "--only-version" : null,
    isDryRun ? "--dry-run" : null,
  ].filter(Boolean);

  const cmd = childProcess.spawn("yarn", args, { stdio: "inherit" });

  process.chdir("../..");

  cmd.on("close", function (code) {
    done(code);
  });
}

function releaseAllPackages(isDryRun, done) {
  // must be master
  const currentBranchName = childProcess
    .execSync("git rev-parse --abbrev-ref HEAD")
    .toString()
    .trim();
  if (currentBranchName !== "main")
    throw "releasing all packages can only be done from the master branch";

  // get a list of tags
  const tags = childProcess
    .execSync("git tag -n")
    .toString()
    .split("\n")
    .map((tag) => tag.split(" ")[0]);

  // get a list of ignored packages
  const ignorelist = fs
    .readFileSync(".releaseignore", "utf8")
    .toString()
    .split("\n")
    .filter((ignored) => !ignored.startsWith("#"));

  // get a list of packages that are not ignored
  const packages = fs
    .readdirSync("packages")
    .filter(
      (package) =>
        !ignorelist.find((ignored) => "packages/" + package === ignored)
    );

  const next = (packages, tags, isDryRun, done) => {
    if (!packages.length) return done();

    // process the first package from the list
    const package = packages.shift();

    // read package.json
    process.chdir("packages/" + package);
    const pkg = getPackageJson();
    process.chdir("../..");

    // get the latest tag
    // by simply sorting all tags alphabetically.
    // this might be a bit naive, but should work for now.
    const pkgTags = tags
      .filter((tag) => tag.startsWith(pkg.name))
      .sort()
      .reverse();
    const latestTag = pkgTags[0];
    const onlyVersion = true;

    if (latestTag) {
      // latest tag found
      // compare diff with HEAD to see if there are any changes since the last release.
      const diff = childProcess
        .execSync(`git diff ${latestTag} HEAD --name-only`)
        .toString()
        .split("\n");
      if (diff.find((name) => name.startsWith("packages/" + package))) {
        // changes found. go ahead with release.
        releasePackage(package, isDryRun, onlyVersion, () => {
          next(packages, tags, isDryRun, done);
        });
      } else {
        // no changes found. skip release.
        console.log(
          `skipping ${pkg.name} because no changes have been made since it's last release.`
        );
        next(packages, tags, isDryRun, done);
      }
    } else {
      // no previous tag found. must be the first release. let's go.
      releasePackage(package, isDryRun, onlyVersion, () => {
        next(packages, tags, isDryRun, done);
      });
    }
  };

  next(packages, tags, isDryRun, done);
}

function buildProject(skipBuild, isDryRun, done) {
  if (skipBuild) {
    console.log("skipping build.");
    return done();
  }
  if (isDryRun) {
    console.log("! building project");
    return done();
  }
  const cmd = childProcess.spawn("yarn", ["build"], { stdio: "inherit" });
  cmd.on("close", function (code) {
    if (code !== 0) {
      throw "error building the project";
    }
    done();
  });
}

function release(done) {
  const { package, dryRun, skipBuild } = require("yargs").argv;

  buildProject(skipBuild, dryRun, () => {
    if (package) {
      // release a single package
      const onlyVersion = false;
      releasePackage(package, dryRun, onlyVersion, done);
    } else {
      // release all packages
      releaseAllPackages(dryRun, done);
    }
  });
}

module.exports = {
  release,
};
